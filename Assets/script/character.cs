﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class character : MonoBehaviour {

    public float movementSpeed = 2f;
    public float sideStep = 1f;
    public float mouseSensitivity = 2f;
    public float upDownRange = 90;
    public float downSpeed = 0;
    private float speedCnt;
    public bool isSmall = false;

    private Vector3 speed;
    private float forwardSpeed;
    private float sideSpeed;

    private float rotLeftRight;
    private float rotUpDown;
    private float verticalRotation = 0f;
    private float gravity = 14.0f;

    private float verticalVelocity;

    private CharacterController cc;
    private float tCamY;

    // Use this for initialization
    void Start()
    {
        cc = GetComponent<CharacterController>();
        cc.Move(Vector3.zero);
        Cursor.lockState = CursorLockMode.Locked;
    }
    // Update is called once per frame
    void Update()
    {

        FPMove();
        FPRotate();
    }

    //Player의 x축, z축 움직임을 담당
    void FPMove()
    {
        speedCnt = movementSpeed;
        forwardSpeed = Input.GetAxis("Vertical") * speedCnt;
        sideSpeed = Input.GetAxis("Horizontal") * speedCnt;

        if (cc.isGrounded)
        {
            verticalVelocity = -gravity * Time.deltaTime;
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }

        speed = new Vector3(sideSpeed, verticalVelocity, forwardSpeed);
        speed = transform.rotation * speed;
        cc.Move(speed * Time.deltaTime);
    }

    //Player의 회전을 담당
    void FPRotate()
    {
        //좌우 회전
        rotLeftRight = Input.GetAxis("Mouse X") * mouseSensitivity;
        transform.Rotate(0f, rotLeftRight, 0f);

        //상하 회전
        verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        verticalRotation = Mathf.Clamp(verticalRotation, -upDownRange, upDownRange);
        Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0f, 0f);
    }
}
