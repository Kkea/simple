﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class countdown : MonoBehaviour {

    float timer = 3.0f;
    float fps;
    float day;
    string dayFont = "Day";

    void Update()
    {
        timer -= Time.deltaTime;
    }

    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();
        GUIStyle dayStyle = new GUIStyle();

        Rect rect = new Rect(0, h * 1 / 100, w, h * 4 / 100);
        Rect dayrect = new Rect(0, h * 1 / 100, w, h * 4 / 100);
        style.alignment = TextAnchor.UpperCenter;
        style.fontSize = h * 4 / 100;
        style.normal.textColor = new Color(1f, 1f, 1f, 1.0f);

        dayStyle.alignment = TextAnchor.UpperRight;
        dayStyle.fontSize = h * 4 / 100;
        dayStyle.normal.textColor = new Color(1f, 1f, 1f, 1.0f);

        float msec = timer;
        if(msec <= -0.5)
        {
            switch (dayFont)
            {
                case "Day":
                    dayFont = "Night";
                    break;
                case "Night":
                    dayFont = "Day";
                    day++;
                    break;
            }
            timer = 3;
        }
        
        string text = string.Format("{0:0}", msec);
        string daytext = string.Format("{0:0} {1:0}" ,dayFont, day);
        GUI.Label(rect, text, style);
        GUI.Label(dayrect, daytext, dayStyle);

    }
}
